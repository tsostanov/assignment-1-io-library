%define READ_CODE 0
%define WRITE_CODE 1
%define EXIT_CODE 60

%define STDIN 0
%define STDOUT 1




section .text


; Принимает код возврата и завершает текущий процесс
exit:
         mov rax, EXIT_CODE
         syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
     xor rax, rax
     .loop:
         cmp byte[rdi+rax], 0
         je .end
         inc rax
         jmp .loop
    .end:
         ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
         push rdi
         call string_length
         mov rdx, rax
         pop rsi
         mov rax, WRITE_CODE
         mov rdi, STDOUT
         syscall
         ret

; Принимает код символа и выводит его в stdout
print_char:
        push rdi
        mov rsi, rsp
        add rsp, 8
        mov rax, WRITE_CODE
        mov rdi, STDOUT
        mov rdx, 1
        syscall
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
        mov rdi, `\n`
        jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, 10
    mov rdi, rsp
    sub rsp, 24
    dec rdi
    mov byte[rdi], 0
    .loop:
         xor rdx, rdx
         div rsi
         add dl, '0'
         dec rdi
         mov byte[rdi], dl
         test rax, rax
         jnz .loop
         call print_string
         add rsp, 24
         ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
        mov al, byte[rsi+rcx]
        cmp byte[rdi+rcx], al
        jne .not_equal
        cmp byte[rdi+rcx], 0
        je .equal
        inc rcx
        jmp .loop
    .equal:
        mov rax, 1
        ret
    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, READ_CODE
    mov rdi, STDIN
    mov rdx, 1
    mov rsi, rsp
    sub rsi, 8
    syscall
    test rax, rax
    je .end
    mov  al, byte[rsi]
    ret
    .end:
        xor rax, rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    xor r14, r14
    mov r12, rsi
    mov r13, rdi
    .skip_space:
        call read_char
        cmp rax, ' '
        je .skip_space
        cmp rax, `\t`
        je .skip_space
        cmp rax, `\n`
        je .skip_space
        test rax, rax
        jz .end
        cmp r14, r12
        jae .end
        mov byte[r13 + r14], al
        inc r14
    .loop:
        call read_char
        cmp  r14, r12
        jae .end
        cmp rax, ' '
        je .happy_end
        cmp rax, `\t`
        je .happy_end
        cmp rax, `\n`
        je .happy_end
        test rax, rax
        jz .happy_end
        mov byte[r13 + r14], al
        inc r14
        jmp .loop
    .happy_end:
        mov rax, r13
        mov rdx, r14
        mov byte[r13 + r14], 0
        pop r14
        pop r13
        pop r12
        ret
    .end:
        pop r14
        pop r13
        pop r12
        xor rdx, rdx
        xor rax, rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    .loop:
        mov sil, byte[rdi]
        cmp sil, '0'
        jb .done
        cmp sil, '9'
        ja .done
        sub sil, '0'
        imul rax, 10
        add rax, rsi
        inc rdx
        inc rdi
        jmp .loop
    .done:
         ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    mov rax, 1                  ;sign
    .another_sign:
        mov sil, byte[rdi]
        cmp sil, '-'
        je .minus
        cmp sil, '+'
        jne .handle_sign
        .minus:
            neg rax
        inc rdi
        jmp .another_sign
    .handle_sign:
        cmp rax, 1
        je .pos
        call parse_uint
        test rdx, rdx
        jz .ending
        neg rax
        inc rdx
        ret
        .ending:
            ret
        .pos:
            call parse_uint
            ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov r10b, byte[rdi+rcx]
        test r10b, r10b
        jz .done
        cmp rax, rdx
        jae .exceed
        mov byte[rsi+rcx], r10b
        inc rcx
        inc rax
        jmp .loop
    .done:
        mov byte[rsi+rcx], 0
        ret
    .exceed:
        xor rax, rax
        ret

